## Logiciels développés

Les bibliothèques et solveurs développés dans l’équipe sont, pour la majorité, accessibles en open-source pour le 
monde académique et aussi le monde socio-économique.
La bibliothèque pour les modèles graphiques aGrUM en est la parfaite illustration en comptant parmi ses utilisateurs 
des académiques et aussi de grands groupes industriels (Airbus, BASF, Engie, etc.).
Les logiciels suivants sont aussi disponibles librement, ils sont soient développés exclusivement dans COALA ou dans le cadre de diverses collaborations :

- [**aGrUM**](https://agrum.gitlab.io/) : une bibliothèque de modèles graphiques.
- [**BenzAI**](https://hal.science/hal-03691364) : un logiciel destiné aux chimistes qui répond à plusieurs questions sur les benzénoïdes en utilisant des techniques d’IA.
- **MS-Builder** / **MS-Checker** : un logiciel de construction et vérification de certificats (preuves) pour Max-SAT.
- [**BTD**](https://github.com/Terrioux/BTD-RBO) : un solveur à base de décomposition arborescente pour CSP. 
- **Kissat-MAB** : un solveur SAT utilisant des techniques d’apprentissage automatique.
