## Présentation de l'équipe COALA

### Mots clés

Intelligence Artificielle, Programmation par Contraintes, Résolution de problèmes combinatoires, Applications Industrielles, 
Conception de Solveurs, Calcul propositionnel, SAT, CSP, Optimisation sous contraintes, Algorithmique, Théorie de la Complexité, 
Théorie Algorithmique des Graphes, Planification automatique, Packing

### Objectif scientifique

Si par nature, les travaux de COALA sont clairement à finalité fondamentale, les thèmes
que nous abordons recèlent néanmoins une très forte potentialité d’applications pratiques.
Aussi, l’équipe COALA a entrepris, depuis sa création, une démarche visant à renforcer ses
interactions à l’extérieur du monde académique. Dans cette démarche, nous avons cependant 
toujours à veillé à ce que ces relations soient positionnées sur nos thèmes de recherche clés. 
Cette stratégie d’ouverture a pour nous plusieurs vertus :

- s’ouvrir vers la société pour faire profiter à la fois de nos connaissances, de nos compétences et de notre savoir-faire;
- diffuser les progrès et les avancées significatives réalisées en recherche ces dernières
  années autour de l’Algorithmique de l’Intelligence Artificielle (p. ex. les solveurs SAT
  ou CSP sont désormais opérationnels dans le cadre industriel 1) dans un contexte où le
  monde socio-éconimique dispose d’une vision unique de l’IA (cf. les succès médiatisés
  du "Deep Learning") occultant l’étendue des progrès réalisés notamment dans notre
  domaine de recherche;
- ne pas rester cantonnés à des problématiques purement théoriques car il est toujours
  souhaitable sur nos thématiques, de se confronter à la réalité du monde;
- participer activement à l’effort de la société en travaillant sur des sujets d’enjeux socié-
  taux d’actualité et majeurs, en lien notamment avec l’environnement, le transport, les
  énergies renouvelables;
- assurer une certaine autonomie de moyens, tant en termes de crédits de fonctionnement
  qu’au niveau des financements de thèses (Cf. 5 financements de thèses sur la périodes
  via cette démarche), de sorte à ne pas être trop tributaire des financements institutionnels par nature limités et 
souvent aléatoires dans leur obtention.
  Cette ouverture s’est principalement orientée vers la mise en place de partenariats avec le
  monde socio-économique sur la base de projets collaboratifs, mais pas exclusivement.
