## Membres de l'équipe

### Responsable 

[Christophe GONZALES Enseignant/Chercheur](mailto:christophe.gonzales[@]lis-lab.fr)

### Membres Actuels
- [AMOKRANE Roza Doctorante](mailto:roza.amokrane[@]lis-lab.fr)
- [COPPE alexandre Doctorant](mailto:alexandre.coppe[@]lis-lab.fr)
- [EL GHAZI Yousra Doctorante](mailto:yousra.el-ghazi[@]lis-lab.fr)
- [GONZALES Christophe Enseignant/Chercheur](mailto:christophe.gonzales[@]lis-lab.fr)
- [GRANDCOLAS Stephane Enseignant/Chercheur](mailto:stephane.grandcolas[@]lis-lab.fr)
- [HABET Djamal Enseignant/Chercheur](mailto:djamal.habet[@]lis-lab.fr)
- [HENOCQUE Laurent Enseignant/Chercheur](mailto:laurent.henocque[@]lis-lab.fr)
- [JÉGOU Philippe Enseignant/Chercheur](mailto:philippe.jegou[@]lis-lab.fr)
- [LI Chu min Enseignant/Chercheur](mailto:chu-min.li[@]lis-lab.fr)
- [LI Shuolin Doctorant](mailto:shuolin.li[@]lis-lab.fr)
- [OSTROWSKI Richard Enseignant/Chercheur](mailto:richard.ostrowski[@]lis-lab.fr)
- [OXUSOFF Laurent Enseignant/Chercheur](mailto:laurent.oxusoff[@]lis-lab.fr)
- [PAIN-BARRE Cyril Enseignant/Chercheur](mailto:cyril.pain-barre[@]lis-lab.fr)
- [PARIS Lionel Enseignant/Chercheur](mailto:lionel.paris[@]lis-lab.fr)
- [PRCOVIC Nicolas Enseignant/Chercheur](mailto:nicolas.prcovic[@]lis-lab.fr)
- [SREEDHARAN Haseena Doctorant](mailto:haseena.sreedharan[@]lis-lab.fr)
- [TERRIOUX Cyril Enseignant/Chercheur](mailto:cyril.terrioux[@]lis-lab.fr)
- [VALIZADEH Amir hosein Doctorant](mailto:amir-hosein.valizadeh[@]lis-lab.fr)

### Anciens Membres

- [CHERIF Mohamed Sami Doctorant](mailto:mohamedsami.cherif[@]lis-lab.fr)
- [VARET Adrien Doctorant](mailto:adrien.varet[@]lis-lab.fr)


