# Projets en cours

### chaire ANR Massal’IA

La chaire ANR en IA Massal’IA, Propositional Reasoning for Large-Scale Optimization :
Application to Clean Energy Mobility Issues, est coordonnée par COALA et a débuté en
septembre 2020 pour une durée de 4 ans. Elle est proposée dans un contexte où la résolution 
pratique des problèmes combinatoires à l’aide des formalismes SAT et Max-SAT
est hautement compétitive, alors qu’il est encore possible et nécessaire d’améliorer les
performances des algorithmes SAT et MaxSAT. Ainsi, l’objectif du projet Massal’IA est
de développer des algorithmes SAT et MaxSAT pour l’optimisation à grande échelle
où les informations peuvent être complexes, incertaines et évolutives dans le temps. Il
s’agira ensuite d’appliquer ces algorithmes au problème de la mobilité des véhicules
électriques à l’échelle de la métropole Aix-Marseille Provence puis au-delà. Un modèle
de données sera également construit, pour la prédiction et la simulation du trafic et à
l’aide à la validation des solutions au problème industriel, à base d’outils de l’IA comme
les réseaux bayésiens, l’apprentissage automatique, etc. Un partenaire industriel majeur, Enedis, est impliqué dans ce projet, dont l’objectif est d’aider au développement de la mobilité électrique notamment sur les aspects liés à la localisation des bornes de
recharge, la planification des réserves d’énergie mobiles, l’exploitation des sources de
production d’énergie renouvelable, etc. Les partenaires sont AMU-LIS, Université Picardie Jules Verne, Enedis et la métropole Aix-Marseille Provence.


### Projet TNTM

Le projet TNTM (Transformation Numérique du Transport Maritime) a été soumis et accepté dans le cadre de l’appel à projets de recherche et développement structurants pour la compétitivité (PSPC) et financé par BPI (Banque public d’investissement) France. Ce projet a pour objectif principal de réduire l’impact environnemental du transport maritime dans le cadre des exigence de l’organisation maritime internationale qui impose qu’en 2050 les émissions de CO2 soient réduites de 70% et celles de des Gaz à Effet de Serre de 50%, par rapport à l’année de référence 2008. Ce projet s’inscrit dans une démarche de R&D collaborative qui vise l’excellence opérationnelle des navires, en misant sur l’apport de la donnée, de la simulation et de l’optimisation des différents processus : modèles de performance de navire, modèle de défaillance/sécurité, modèles de conditions opérationnelles des conteneurs frigorifiques, modèles d’efficience de transport conteneurisé, d’optimisation de la flotte, de remplissage de porte conteneurs et de routage optimal des navires. Le projet a une durée de 48 mois (démarrage officiel décalé à janvier 2022 2, avec financement rétrospectif possible des dépenses déjà engagées dans le cadre du projet). Les partenaires de ce projets sont : AMU-LIS, École centrale de Nantes, Ifremer, CMA-CGM, Predict, Bureau Veritas (solutions) et Traxens. TNTM est structuré autour de 8 lots différents. COALA coordonne les 3 lots suivants : 

1) Optimisation de la flotte de navires, 
2) Optimisation du remplissage de portes containers
3) Routage environnemental optimal des navires. 

Il est notamment prévu pour chacun des lots le recrutement d’un doctorant et d’un ingénieur de recherche sur 2 ans. Enfin, la quasi-totalité des permanents de l’équipe participent à ce projet.


# Projets terminés

### CIFRE LIS/COALA – Engie (Lab CRIGEN)

CIFRE LIS/COALA – Engie (Lab CRIGEN), Axel Journe (depuis décembre 2019) : Au sein d’un environnement confus, 
complexe et aux enjeux multiples, le décideur fait généralement appel à plusieurs bases de données et de connaissances 
pour l’aider dans son processus de prise de décision. Cependant, quand le problème décisionnel est complexe et de 
surcroît mal défini, le décideur éprouve de plus en plus de difficultés à comprendre les données qu’il a à sa disposition, 
ce qui complique par conséquent le processus de prise de décision. C’est déjà le cas pour la plupart des problèmes étudiés chez ENGIE. 
Dans ce cadre, l’objectif principal de la thèse consiste à mettre en place une solution logicielle et algorithmique valorisant les données 
collectées et permettant ainsi d’expliciter les problèmes décisionnels complexes et mal définis avec une application à l’exploitation 
des éoliennes.

### ANR SunStone

L’objectif de l’ANR SunStone, dont COALA est l’un des membres de son consortium
par l’intermédiaire de C. Gonzales, est le pilotage optimal de réseaux de chaleurs urbains. 
Par optimal, on entend que l’on minimise le prix du KWh produit sur une année. Plus précisément 
la chaleur est produite par des chaudières biomasse (bois) et gaz,
ainsi que par des capteurs solaires qui chauffent un ballon d’eau chaude (pour une
consommation à court terme) ou des champs de sondes (qui conservent la chaleur pour
une utilisation à moyen/long terme). L’objectif est donc de trouver la manière optimale
d’utiliser ces différentes sources. Les partenaire de ce projet sont : AMU-LIS, le Centre
d’énergétiques et de thermique de Lyon, Laboratoire de recherche en informatique de
Sorbonne Université (LIP6), Bureau de Recherches Géologiques et Minières (BRGM) et
TECSOL (entreprise de technologies innovantes dans les secteurs de l’énergie solaire).
